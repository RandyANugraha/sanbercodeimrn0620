console.log("LOOPING PERTAMA");

var angka = true
var jatahangka = 20
var stockangka = 2

while(angka) {
    console.log(stockangka + " - I Love Coding");
    if(jatahangka == stockangka){
        angka = false
    }
    stockangka += 2;
}

console.log("LOOPING KEDUA");

var angka = true
var jatahangka = 2
var stockangka = 20

while(angka) {
    console.log(stockangka + " - I will become a mobile developer");
    if(jatahangka == stockangka){
        angka = false
    }
    stockangka -= 2;
}

console.log("OUTPUT");

for (var i = 1; i <= 20; i++){
    if(i % 2 == 0 ){
        console.log(i + " - Berkualitas");
    } else if(i % 3 == 0 && 1 % 2 != 0) {
        console.log(i + " - I Love Coding");
    } else {
        console.log(i + " - Santai");
    }
}

var kotak8 = "########"

for (var i = 1; i <= 4; i++){
    console.log(kotak8)
}

var no1 = "#"
var no2 = "#######"
var angka = true

while(angka) {
    console.log(no1);
    if(no2 == no1){
        angka = false
    }
    no1 += "#";
}

for (var i = 1; i <= 8; i++){
    if(i % 2 == 0 ){
        console.log("# # # # ");
    } else {
        console.log(" # # # #");
    }
}